FROM nodesource/node:4.0

ADD package.json package.json
ADD bower.json bower.json

RUN echo '{ "allow_root": true }' > /root/.bowerrc

RUN npm install
RUN npm install -g bower
RUN bower install
ADD . .

CMD ["node","index.js"]
