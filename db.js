var promisify = require('promisify-node');
var bcrypt = require('bcrypt');

module.exports = function(path) {
	var sqlite3 = require('sqlite3').verbose();
	var db = new sqlite3.Database(path);

	return {
		db: db,
		userById: function(id, fn) {
			var self = this;
			db.serialize(function () {
				db
					.prepare('SELECT * FROM users WHERE id = ? LIMIT 1')
					.all(id, self._first(fn, 'User not found'));
			});
		},
		userByUsername: function(username, fn) {
			var self = this;
			db.serialize(function () {
				db
					.prepare('SELECT * FROM users WHERE username = ? LIMIT 1')
					.all(username, self._first(fn, 'User not found'));
			});
		},
		/**
		 * Fetches a user by username and compares the passwords,
		 *  if they match, the user will be returned, otherwise an error
		 *  passed to the callback function.
		 */
		verifyLogin: function(username, password, fn) {
			var self = this;
			self.userByUsername(username, function(err, user) {
				if (err || !user) {
					return fn(err, null);
				}

				bcrypt.compare(password, user.password, function(err, res) {
					if (res === true) {
						fn(null, user);
					} else {
						fn("Invalid Password", false);
					}
				});
			});
		},
		/**
		 * Inserts a new User into the database.
		 */
		createUser: function(username, displayname, password, fn) {
			var self = this;
			promisify(bcrypt.genSalt)(10)
			.then(function (salt) {
				return promisify(bcrypt.hash)(password, salt)
			})
			.then(function (hash) {
				db.serialize(function() {
					db.prepare('INSERT INTO users (username, displayname, password) VALUES (?, ?, ?)')
							.run([username, displayname, hash], function(err) {
								if (err) return fn(err);
								// pass new user to the caller
								self.userByUsername(username, fn);
							});
				});
			}).catch(function(err) {
				fn(err);
			})
		},
		getUsers: function(fn, limit) {
			if (!limit) {
				limit = 500;
			}
			db.serialize(function() {
				db.prepare('SELECT * FROM users LIMIT ?;').all(limit, fn);
			});
		},
		migrate: function() {
			db.serialize(function () {
				db.run('CREATE TABLE IF NOT EXISTS users ( \
					id INTEGER PRIMARY KEY AUTOINCREMENT, \
					username VARCHAR(255) UNIQUE, \
					displayname VARCHAR(255) NOT NULL, \
					password VARCHAR(255) NOT NULL, \
					role INTEGER DEFAULT 0 \
				);');
				db.run('CREATE TABLE IF NOT EXISTS points (\
					id INTEGER PRIMARY KEY AUTOINCREMENT, \
					user_id INTEGER NOT NULL, \
					value INTEGER NOT NULL, \
					timestamp INT(8) NOT NULL, \
					FOREIGN KEY(user_id) REFERENCES users(id) \
				);')
				db.run('CREATE TABLE IF NOT EXISTS migrations ( \
					id INTEGER PRIMARY KEY AUTOINCREMENT, \
					timestamp INT(8) NOT NULL, \
					migration_id INT(8) NOT NULL UNIQUE \
				);');

				/*db.serialize(function() {
					db.prepare('SELECT migration_id FROM migrations;').all(function(ids) {

					});
				});*/

				console.log('Migration complete!');
			});
		},
		seed: function() {
			var errFn = function(err) {
				if (err) {
					console.log("Seeding Error", err);
				}
			};
			this.createUser('5gaertne', 'Christian', 'asdf', errFn);
			this.createUser('4gaertne', 'Peter', 'asdf', errFn);
		},
		_first: function(fn, errmsg) {
			return function(err, rows) {
				if (err) {
					return fn(err);
				}
				if (rows.length == 0) {
					if (errmsg) {
						return fn(errmsg);
					} else {
						return fn("Not Found");
					}
				}
				fn(err, rows[0]);
			};
		}
	}
}
