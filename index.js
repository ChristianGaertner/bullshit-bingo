var express = require('express');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var passportSocketIo = require('passport.socketio');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var morgan = require('morgan')
var RoomManager = require('./roommanager')

var path = process.env.OPENSHIFT_DATA_DIR || 'storage'; 

var db = require('./db')(path + '/db.sqlite');

var roomManager = new RoomManager();

db.migrate();
//db.seed();

var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000;
var ip = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

var SQLiteStore = require('connect-sqlite3')(session);

var sessionStore = new SQLiteStore({
	table: 'sessions',
	dir: path
});

app.set('views', __dirname + '/views');
app.set('layout', 'layout');
app.set('view engine', 'ejs');

app.use(require('express-ejs-layouts'));
app.use(cookieParser());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(express.static('public'))
app.use(express.static('bower_components'))
app.use(morgan('combined'))
app.use(session({
	key: 'express.sid',
	secret: 'CHANGE_ME',
	resave: false,
	saveUninitialized: false,
	store: sessionStore
}));


passport.use(new Strategy(function(username, password, cb) {
	db.verifyLogin(username, password, cb);
}));

passport.serializeUser(function(user, cb) {
	cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
	db.userById(id, cb);
});

app.use(passport.initialize());
app.use(passport.session());

require('./router')(app, io, roomManager, db)

require('./socket')(io, passportSocketIo.authorize({
	cookieParser: cookieParser,
	key: 'express.sid',
	secret: 'CHANGE_ME',
	store: sessionStore
}), roomManager);



http.listen(port, ip, function () {
	console.log('Listening on ' + ip + ':' + port)
});