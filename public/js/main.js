function init() {
	var socket = io();

	$('#ready').click(function() {
		start(socket);
	});

	// enable draggable
	REDIPS.drag.init()

	//enable recommendation word cloud
	$('#recommendations').jQCloud([
		{ text: 'Eigenkapital', weight: 5 },
		{ text: 'Fremdkapital', weight: 6 },
		{ text: 'Aktien', weight: 7 },
		{ text: 'Kostenstelle', weight: 8 },
		{ text: 'Kennzahlen', weight: 5 },
		{ text: 'Kostenart', weight: 4 },
	], {
		height: 300,
		steps: 10
	});

	socket.on('initial user list', function (room, users) {
		$('#userlist').empty();

		for (var id in users) {
			pushUser(users[id]);
		}
	});

	socket.on('user join', function (user) {
		$.notify({
			// options
			message: '<strong>' + user.username + '</strong> joined.' 
		},{
			// settings
			type: 'info'
		});
		pushUser(user);
	});

	socket.on('saved card', function(card, punches) {
		setCard('plan', card);
		if (punches) {
			start(socket);
		}
		for (var flatCoord in punches) {
			coords = [Math.floor(flatCoord / 5), flatCoord % 5];
			punch(coords);
		}
	});

	$('.chat form').submit(function () {
		if (!$('#msg').val()) return false;
		
		socket.emit('chat message', $('#msg').val());
		$('#msg').val('');
		return false;
	});

	socket.on('chat message', function (user, msg) {
		var list = $('#messages');
		list.append('<li><span class="username">' + user.username + '</span>' + msg + '</li>');
		list.animate({scrollTop: list.prop('scrollHeight') - list.height()}, 50);
	});

	socket.on('bingo punch', punch);

	socket.on('bingo punched', function (value, user) {
		$.notify({
			// options
			message: '<strong>' + user.username + '</strong> just punched <i>' + value + '</i>' 
		},{
			// settings
			type: 'info'
		});
	});

	socket.on('bingo', function (user) {
		$.notify({
			// options
			message: 'BINGO! <strong>' + user.username + '</strong> won.' 
		},{
			// settings
			type: 'danger'
		});
	});

	socket.on('won', onWin);

	socket.on('ready', function(user) {
		$.notify({
			// options
			message: '<strong>' + user.username + '</strong> started playing.' 
		},{
			// settings
			type: 'info'
		});
	});

	socket.on('bingo unpunch', function(value, coords, user) {
		var cell = $('.bingo-card tbody tr').eq(coords[0]).children()[coords[1]];
		cell.className.replace('/\bpunched\b/', '');
	});
}

function start(socket) {

	// submit bingo-card
	var values = $('.bingo-card.plan td input').map(function(_, element) {
		return element.value;
	});

	setCard('live', values);

	// wait for punches
	$('.bingo-card tbody tr').children().not('#joker').click(function (e) {
		// if the cell is empty. do not punch it
		if (!e.target.textContent) {
			return;
		}
		socket.emit('bingo punch', getIndex(e.target));
	});

	// broadcast ready
	socket.emit('ready', values.toArray());

	// disable ready button
	$('#ready').prop('disabled', true);
	$('#ready').html('Ready!');

	$('.bingo-card.live').show();
	$('.bingo-card.plan').hide();
}

function punch(coords) {
	var cell = $('.bingo-card tbody tr').eq(coords[0]).children()[coords[1]];
	cell.className += ' punched';
}

function setCard (type, values) {
	var cssSelector = '#MISSING_SELECTOR';

	if (type === 'live') {
		cssSelector = '.bingo-card.live td';
	} else if (type === 'plan') {
		cssSelector = '.bingo-card.plan td input';
	}

	$(cssSelector).not('#joker').each(function(index, element) {
		if (element.nodeName === 'INPUT') {
			element.value = values[index];
			return;
		}

		element.textContent = values[index];
	});
}

function getIndex(el) {
	var target = $(el);
	var row = target.parent().index();
	var col = target.index();
	return [row, col];
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function pushUser(user) {
	$('#userlist')
		.append('<li><span class="displayname">' + user.displayname +
			'</span> (<span class="username">' + user.username + 
			')</span></li>')
}

function onWin(newWin) {
	if (newWin) {
		$('#bingo_popup').fadeIn().delay(2000).fadeOut();
		
		// var audio = new Audio('audio_file.mp3');
		// audio.play();
	}

	$('.bingo-card tbody tr').children().not('#joker').off("click").click(function() {
		$.notify({
			// options
			message: 'You already won. You can leave and rejoin, allowing you to play again.' 
		},{
			// settings
			type: 'danger'
		});
	});
}