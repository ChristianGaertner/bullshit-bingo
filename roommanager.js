var readableRandom = require('readable-random')
/*
Room schema
{
	symbol,
	displayname,
	owner,
	users[ id -> user ],
	status[user_id -> status ], (status in WON, PLAYING, PLANING)
	cards[ user_id -> card ],
	punches [ user_id -> punches ],
	password,
	locked,
	created,
}

inverse schema:
user.id -> room_symbol
*/

module.exports = function() {

	// maps symbol to room object.
	var rooms = {};
	var inverse = {};

	return {
		create: function(name, user, password) {
			if (user.id in inverse) {
				this.leave(inverse[user.id], user);
			}

			var symbol = readableRandom.getString(8);
			
			while (symbol in rooms) {
				symbol = readableRandom.getString(8);
			}

			var r = {
				symbol: symbol,
				displayname: name,
				owner: user,
				users: {},
				status: {},
				cards: {},
				punches: {},
				password: password,
				locked: false,
				created: Date.now()
			}
			rooms[symbol] = r;
			this.join(symbol, user);
			return r;
		},
		exists: function(symbol) {
			return symbol in rooms;
		},
		join: function(symbol, user) {
			if (!this.exists(symbol)) {
				return false;
			}
			if (this.contains(symbol, user)) {
				return true;
			}
			if (user.id in inverse) {
				this.leave(inverse[user.id], user);
			}

			rooms[symbol].users[user.id] = user;
			inverse[user.id] = symbol;
			return true;
		},
		leave: function(symbol, user) {
			if (!this.exists(symbol)) {
				return false;
			}

			delete rooms[symbol].users[user.id];
			delete rooms[symbol].cards[user.id];
			delete rooms[symbol].punches[user.id];
			delete rooms[symbol].status[user.id];

			if (user.id in inverse) {
				delete inverse[user.id];
			}
		},
		setCard: function(symbol, user, card) {
			if (!this.contains(symbol, user)) {
				throw new "User not in room";
			}

			this.getBySymbol(symbol).cards[user.id] = card; 
		},
		setStatus: function(symbol, user, status) {
			this.getBySymbol(symbol).status[user.id] = status;
		},
		getStatus: function(symbol, user) {
			if (user.id in rooms[symbol].status) {
				return this.getBySymbol(symbol).status[user.id];
			}
			return 'PLANING'
		},
		contains: function(symbol, user) {
			if (!this.exists(symbol)) {
				return false;
			}
			return user.id in rooms[symbol].users;
		},
		getBySymbol: function(symbol) {
			return rooms[symbol];
		},
		delete: function(symbol) {
			for (id in rooms[symbol].users) {
				this.leave(symbol, rooms[symbol].users[id])
			}
			delete rooms[symbol];
		},
		put: function(symbol, room) {
			rooms[symbol] = room;
		},
		lock: function(symbol) {
			rooms[symbol].locked = true;
		},
		unlock: function(symbol) {
			rooms[symbol].locked = false;
		},
		getSymbolForUser: function(user) {
			if (user.id in inverse) {
				return inverse[user.id];
			} else {
				return false;
			}
		},
		/**
		 * Remove rooms older than 10 hours.
		 */
		clearOld: function() {
			var ten_hours = 1000 * 60 * 60 * 10;
			var now = Date.now();
			for (symbol in rooms) {
				if (rooms[symbol].created + ten_hours < now) {
					this.delete(symbol);
				}
			}
		},
		_getAll: function() {
			return rooms;
		}
	}

};