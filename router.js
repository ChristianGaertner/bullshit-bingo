var passport = require('passport');
var guard = require('connect-ensure-login');


module.exports = function (app, io, roomManager, db) {
	app.get('/login', ensureLoggedOut, function (req, res) {
		res.render('login');
	});

	app.get('/', function(req, res) {
		res.redirect('/start');
	});

	app.get('/start', guard.ensureLoggedIn(), function (req, res) {
		if (roomManager.getSymbolForUser(req.user)) {
			res.redirect('play');
			return;
		}
		res.render('start', { user: req.user, rooms: roomManager._getAll() });
	});

	app.post('/create', guard.ensureLoggedIn(), function (req, res) {
		if (roomManager.getSymbolForUser(req.user)) {
			res.redirect('play');
			return;
		}
		var room = roomManager.create(req.body.name, req.user, req.body.password);

		res.redirect('play');
	});

	app.post('/join', guard.ensureLoggedIn(), function(req, res) {
		if (roomManager.getSymbolForUser(req.user)) {
			res.redirect('play');
			return;
		}
		var symbol = req.body.symbol;
		if (!roomManager.exists(symbol)) {
			res.redirect('start?notFound');
			return;
		}

		var room = roomManager.getBySymbol(symbol);
		if (room.password && room.password != req.body.password) {
			res.redirect('start?invalidPassword');
			return;	
		}

		roomManager.join(symbol, req.user);
		io.to(symbol).emit('user join', req.user);
		res.redirect('play');
	});

	app.post('/leave', guard.ensureLoggedIn(), function(req, res) {
		var symbol = req.body.symbol;
		if (!roomManager.exists(symbol)) {
			res.redirect('start');
			return;
		}
		roomManager.leave(symbol, req.user);
		io.to(symbol).emit('user leave', req.user);
		res.redirect('start');
	});

	app.get('/play', guard.ensureLoggedIn(), function (req, res) {
		if (!roomManager.getSymbolForUser(req.user)) {
			res.redirect('start');
			return;
		}
		var room = roomManager.getBySymbol(roomManager.getSymbolForUser(req.user));
		if (!room) {
			res.redirect('start');
			return;	
		}
		res.render('play', { user: req.user, room: room });
	});

	app.get('/register', ensureLoggedOut, function (req, res) {
		res.render('register');
	});

	app.post('/register', ensureLoggedOut, function (req, res) {
		var username = req.body.username;
		var displayname = req.body.displayname;
		var password = req.body.password;

		if (username.length < 2) {
			res.redirect('/register?err=' + encodeURIComponent('Username is too short'));
			return;
		}

		if (displayname.length < 3) {
			res.redirect('/register?err=' + encodeURIComponent('Displayname is too short'));
			return;
		}

		if (password.length < 3) {
			res.redirect('/register?err=' + encodeURIComponent('Password is too short'));
			return;
		}

		if (password != req.body.password_confirm) {
			res.redirect('/register?err=' + encodeURIComponent('Password didn\'t match'));
			return;
		}

		db.createUser(username, displayname, password, function(err, user) {
			if (err) {
				var error = String(err);
				if (error.indexOf("UNIQUE") > -1) {
					res.redirect('/register?err=' + encodeURIComponent('Username already taken!'));
				} else {
					res.redirect('/register?err=' + encodeURIComponent('Mhh... something\'s wrong... Don\'t know what... to be honest.'));
				}
				return;
			}
			res.redirect('/');
		});
	});

	app.post('/login', passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/login'
	}));

	app.get('/logout', guard.ensureLoggedIn(), function (req, res) {
		req.logout();
		res.redirect('/login');
	});



	// ADMIN ROUTES
	app.all('/admin/*', requireAdmin, function(req, res, next) {
		next();
	});

	app.get('/admin', function(req, res) {
		res.render('admin');
	});

	app.get('/admin/users', function (req, res) {
		db.getUsers(function(err, users) {
			res.send(users);
		});
	});

	app.get('/admin/room', function (req, res) {
		res.send(Object.keys(roomManager._getAll()));
	});

	app.get('/admin/room/:symbol', function (req, res) {
		res.send(roomManager.getBySymbol(req.params.symbol));
	});

	app.post('/admin/clearOld', function(req, res) {
		roomManager.clearOld();
		res.send({
			success: true
		});
	});
}

function requireAdmin(req, res, next) {
	if (req.user && req.user.username == '5gaertne') {
		next();
	} else {
		res.redirect('/');
	}
}

function ensureLoggedOut (req, res, next) {
	if (req.user) {
		res.redirect('/');
	} else {
		next();
	}
}