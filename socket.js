module.exports = function (io, middlewear, roomManager) {
	io.use(middlewear);

	io.on('connection', function(socket) {
		console.log(socket.request.user.id + ' connected');

		// join room of the user
		var symbol = roomManager.getSymbolForUser(socket.request.user);
		if (!symbol) {
			// sending malious requests...
			// the js for this hasn't be served by this server.
			socket.emit('c_err', 403);
			return;
		}
		console.log(socket.request.user.id + ' => ' + symbol);
		socket.join(symbol);
		var room = roomManager.getBySymbol(symbol);
		if (!room) {
			socket.emit('c_err', 500);
			return;
		}

		// set to PLANING on connect, when PLAYING (to allow editing)!
		// if won, send it to the client
		if (roomManager.getStatus(symbol, socket.request.user) == 'WON') {
			socket.emit('won', false); // false indicates "no new win".	
		} else {
			roomManager.setStatus(symbol, socket.request.user, 'PLANING');
		}

		socket.emit('initial user list', room, room.users);
		
		if (socket.request.user.id in room.cards) {
			socket.emit('saved card',
				room.cards[socket.request.user.id],
				room.punches[socket.request.user.id]
				);
		}

		socket.on('disconnect', function () {
			console.log(socket.request.user.id + ' disconnected');
		});

		// Chat
		socket.on('chat message', function (msg) {
			if (!msg) return;
			var symbol = roomManager.getSymbolForUser(socket.request.user);
			io.to(symbol).emit('chat message', socket.request.user, msg);
		});

		socket.on('ready', function(card) {
			var symbol = roomManager.getSymbolForUser(socket.request.user);
			if (roomManager.getStatus(symbol, socket.request.user) != 'PLANING') {
				socket.emit('c_err', 403);
				return;
			}
			console.log(socket.request.user.id + " saving card");
			roomManager.setCard(symbol, socket.request.user, card);

			socket.broadcast.to(symbol).emit('ready', socket.request.user);
			roomManager.setStatus(symbol, socket.request.user, 'PLAYING');
		});

		socket.on('bingo punch', function(coords) {
			var uid = socket.request.user.id;
			var symbol = roomManager.getSymbolForUser(socket.request.user);
			var room = roomManager.getBySymbol(symbol);
			var flatCoords = coords[0] * 5 + coords[1];

			if (roomManager.getStatus(symbol, socket.request.user) != 'PLAYING') {
				socket.emit('c_err', 403);
				return;
			}

			if (!room.punches[uid]) {
				room.punches[uid] = {};
			}
			room.punches[uid][flatCoords] = true;
			socket.emit('bingo punch', coords);
			socket.broadcast.to(symbol).emit('bingo punched', room.cards[uid][flatCoords], socket.request.user);

			if (checkBingo(room.punches[uid])) {
				socket.broadcast.to(symbol).emit('bingo', socket.request.user);
				roomManager.setStatus(symbol, socket.request.user, 'WON');
				socket.emit('won', true);
				// TODO: score points in database.
			}
		});
	});
}

function checkRow(punches, rowIndex) {
	var ok = true;
	for (var i = rowIndex * 5; i < (rowIndex + 1) * 5; i++) {
		ok = ok && (punches[i] || i === 12) // i == 12 for the joker!
	};
	return ok;
}

function checkDiagonals(punches) {
	var ok = true;
	// top-left bottom-right
	for (var i = 0; i < 25; i += 6) {
		ok = ok && (punches[i] || i === 12)
	};
	if (ok) {
		return true;
	}

	ok = true;
	// bottom-left top-right
	for (var i = 20; i > 3; i -= 4) {
		ok = ok && (punches[i] || i === 12)
	};
	return ok;
}

function checkBingo (punches) {
	var rows = false;
	for (var i = 0; i < 5; i++) {
		rows = rows || checkRow(punches, i);
	};
	rows = rows || checkDiagonals(punches);
	return rows;
}